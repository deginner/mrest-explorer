"use strict";

var english = require('./en/app.json');
var ptBR = require('./pt-BR/app.json');

var i18n = {
  en: {
    code: "en",
    locales: ["en-US"],
    messages: english
  },
  ptBR: {
    code: "ptBR",
    locales: ["pt-BR"],
    messages: ptBR
  }
};

var formats = {
  date: {
    withTime: {
      day: "numeric",
      month: "long",
      year: "numeric",
      hour: "numeric",
      minute: "numeric"
    }
  },
  time: {
    hhmm: {
      hour: "numeric",
      minute: "numeric"
    }
  }
};

i18n.defaultLocale = i18n.en;

var order = ['en', 'ptBR'];
i18n.displayOrder = [];

order.forEach(function(code) {
  /* Helpful to keep empty string -> empty string in order to
   * avoid conditionals. */
  i18n[code].messages[''] = '';

  i18n[code].formats = formats;

  i18n.displayOrder.push({
    code: i18n[code].code,
    name: i18n[code].messages.lang[code + "_name"]
  });
});

module.exports = i18n;
