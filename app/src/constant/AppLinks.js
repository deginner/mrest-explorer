var linkName = {
  index: 'welcome',
  info: 'info'
};

var linkList = [
  {
    link: linkName.info,
    i18n: "link.info",
    parent: null    /* Top level link. */
  },
  {
    link: "logout",
    i18n: "link.logout",
    parent: null,
    separate: true
  }
];

var linkIndex = {};
for (var i = 0; i < linkList.length; i++) {
  var item = linkList[i];
  linkIndex[item.link] = i;
}

module.exports = {
  list: linkList,
  linkIndex: linkIndex,
  name: linkName
};
