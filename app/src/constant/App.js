var consts = {
  log: {
    ns: 'deginner'
  },

  pwdMinlen: 9,
  keyparams: {
    cipher: 'aes',
    mode: 'gcm',
    iter: 32000,
    ks: 128,
    ts: 128
  },

  invoice: {
    duration: 7 * 60  /* 7 minutes. */
  }
};

module.exports = consts;
