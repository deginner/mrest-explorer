"use strict";

var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var AppDispatcher = require('../dispatcher');
var EventConstants = require('../constant/EventConstants');
var storage = require('./storage');
var tempstore = require('./tempstore');
var debug = require('../debug')(__filename);
//var mrest = require('mrest-api-client');

var _serverInfo;


function _initialServerInfo() {
  return {
    name: 'server name goes here', /* the server name */
    schemas: {},      /* The server's json schemas */
    keyring: [],  /* The server's keyring */
    userModel: ''
  };
}

/* Load info stored previously. */
_serverInfo = assign({}, _initialServerInfo(),
                    storage.info.get(),
                    tempstore.get());


function update(obj) {
  if (obj.name) {
    /* got server info, store data locally. */
    storage.info.save(obj.name, obj.schemas, obj.keyring, obj.userModel);
  }

  /* Merge objects. */
  _serverInfo = assign(_serverInfo, obj);
}


var InfoStore = assign({}, EventEmitter.prototype, {

  getData: function() {
    return _serverInfo;
  },

  emitChange: function() {
    this.emit(EventConstants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(EventConstants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(EventConstants.CHANGE_EVENT, callback);
  }
});


InfoStore.dispatchToken = AppDispatcher.register(function(action) {

  switch (action.actionType) {

    case 'info-load': {
      debug('load server info');
      update({
        name: action.name,
        schemas: action.schemas,
        keyring: action.keyring,
        userModel: action.user_model
      });
      InfoStore.emitChange();
      break;
    }
  }
});

// TODO port this from ticker store...
/*function getInfo() {
  mrest.get(
    '/ticker/' + pair, null,
    function(ticker) {
      _ticker[pair] = ticker;
      TickerStore.emitChange();
    },
    function(err, info) {
      console.log('err get ticker', err, info);
      _timer = setTimeout(getTicker, 5000);
    }
  );
}

getInfo();*/

module.exports = InfoStore;
