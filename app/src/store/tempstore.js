/* Store data using sessionStorage. */
"use strict";

var store = require('store2');
var user = store.namespace('user');


/* XXX This could be using user.session.getAll() / user.session.setAll()
 * to store data in sessionStorage (otherwise this is not actually a
 * temporary storage). To use that, at a minimum a dialog
 * asking for user credentials need to be added.
 */

function get() {
  return user.getAll();
}

function erase() {
  user.clearAll();
}

function save(data) {
  user.setAll(data);
}


module.exports = {
  get: get,
  save: save,
  erase: erase
};
