"use strict";

var store = require('store2');


var _key = {
  locale: 'user.cfg',
  user: 'user.account',
  info: 'server.info'
};


function _storeValue(key, value) {
  if (store.isFake()) {
    /* XXX Display error to the user to indicate that
     * the local storage is not available. Safari disables
     * it when using private browsing.
     */
    console.log('local storage disabled!!', key);
    return;
  }
  store.set(key, value);
}


var locale = {
  key: _key.locale,

  save: function(key) {
    _storeValue(this.key, {lang: key});
  },

  get: function() {
    return store.get(this.key) || {};
  }
};


var user = {
  key: _key.user,

  save: function(username) {
    _storeValue(this.key, {username: username});
  },

  get: function() {
    var data = store.get(this.key) || {};
    if (typeof data !== "object") {
      /* XXX Maybe give a chance to the user grab this data before
       * erasing it locally.
       */
      console.log("bad data stored in", this.key, "erasing..");
      this.erase();
      data = {};
    }
    return data;
  },

  erase: function() {
    store.remove(this.key);
  }
};


var info = {
  key: _key.info,

  save: function(name, schemas, keyring, userModel) {
    _storeValue(this.key, {name: name, schemas: schemas, keyring: keyring, userModel: userModel});
  },

  get: function() {
    var data = store.get(this.key) || {};
    if (typeof data !== "object") {
      /* XXX Maybe give a chance to the user grab this data before
       * erasing it locally.
       */
      console.log("bad data stored in", this.key, "erasing..");
      this.erase();
      data = {};
    }
    return data;
  },

  erase: function() {
    store.remove(this.key);
  }
};


module.exports = {
  locale: locale,
  user: user,
  info: info
};
