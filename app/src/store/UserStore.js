"use strict";

var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var AppDispatcher = require('../dispatcher');
var EventConstants = require('../constant/EventConstants');
var storage = require('./storage');
var tempstore = require('./tempstore');
var debug = require('../debug')(__filename);

var _userInfo;


function _initialUserInfo() {
  return {
    username: '', /* Username */
    uid: '',      /* UID derived from key; sent to the server. */
    pubhash: '',  /* The bitcoin address that serves as API key. */
    privkey: '',  /* Private key; never sent to the server. */
    mrestClient: '', /* The initialized mrest client. Obviously stays local. */
    status: {     /* Updated during the submit process. */
      info: '',
      error: false
    },
    newuser: false  /* Was this account just created? */
  };
}

/* Load user info stored previously. */
_userInfo = assign({}, _initialUserInfo(),
                    storage.user.get(),
                    tempstore.get());


function update(obj) {
  if (obj.username) {
    /* User is now logged in, store data locally. */
    storage.user.save(obj.username);
  }

  if (obj.privkey || obj.uid) {
    tempstore.save({uid: obj.uid, privkey: obj.privkey});
  }

  /* Merge objects. */
  _userInfo = assign(_userInfo, obj);
}


function logout() {
  storage.user.erase();
  tempstore.erase();
  _userInfo = _initialUserInfo();
}


var UserStore = assign({}, EventEmitter.prototype, {

  getData: function() {
    return _userInfo;
  },

  noNewUser: function() {
    _userInfo.newuser = false;
  },

  emitChange: function() {
    this.emit(EventConstants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(EventConstants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(EventConstants.CHANGE_EVENT, callback);
  }

});


UserStore.dispatchToken = AppDispatcher.register(function(action) {

  switch (action.actionType) {

    case 'login-ok': {
      debug('server accepted user');
      update({
        username: action.username,
        uid: action.uid,
        pubhash: action.pubhash,
        privkey: action.privkey,
        mrestClient: action.mrestClient,
        status: {
          info: '',
          error: false
        },
        newuser: action.newuser ? true : false
      });
      UserStore.emitChange();
      break;
    }

    case 'user-status': {
      update({
        status: {
          info: action.info,
          error: action.error === true
        }
      });
      UserStore.emitChange();
      break;
    }

    case 'logout': {
      debug('received action logout');
      logout();
      UserStore.emitChange();
      break;
    }
  }

});

module.exports = UserStore;
