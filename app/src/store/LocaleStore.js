"use strict";

var assign = require('object-assign');
var EventEmitter = require('events').EventEmitter;
var AppDispatcher = require('../dispatcher');
var EventConstants = require('../constant/EventConstants');
var debug = require('../debug')(__filename);
var storage = require('./storage');

var locales = require('../../locale/messages');
var currentLocale;


function updateLocale(key) {
  var newLocale = require('../../locale/messages')[key];

  if (newLocale) {
    /* Valid key, update. */
    currentLocale = newLocale;
    storage.locale.save(key);
    return key;
  }
}


function _matchLanguage(list) {
  for (var i = 0; i < locales.displayOrder.length; i++) {
    var code = locales.displayOrder[i].code;
    for (var j = 0; j < list.length; j++) {
      if (code === list[j]) {
        return list[j];
      }
    }
  }
}


function _loadInitialLocale(userList) {
  var toLoad;
  var key = storage.locale.get().lang;
  currentLocale = locales.defaultLocale;

  if (key) {
    /* User defined a language previously. */
    if (key !== currentLocale.code) {
      toLoad = key;
    }
  } else {
    /* There is no information about a previous language selection.
     * Check if there is a better choice than the default one based
     * on the list of preferences received.
     */
    var match = _matchLanguage(userList);
    if (match && match !== currentLocale.code) {
      toLoad = match;
    }
  }

  if (toLoad) {
    /* Try to load some locale now. */
    if (!updateLocale(toLoad)) {
      /* But the language does not exist, update storage.
       * This should happen only if a translation gets removed or if
       * the user modifies the locale storage manually.
       */
      debug('bad language', toLoad);
      storage.locale.save(currentLocale.code);
    }
  }
}


var LocaleStore = assign({}, EventEmitter.prototype, {

  getLocale: function() {
    return currentLocale;
  },

  localeOrder: function() {
    return locales.displayOrder;
  },

  emitChange: function() {
    this.emit(EventConstants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(EventConstants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(EventConstants.CHANGE_EVENT, callback);
  }

});


LocaleStore.dispatchToken = AppDispatcher.register(function(action) {

  switch (action.actionType) {

    case 'change-locale': {
      debug('received action', action.locale, currentLocale.code);
      if (action.locale !== currentLocale.code) {
        debug('updating to', action.locale);
        updateLocale(action.locale);
        LocaleStore.emitChange();
      } else {
        debug('ignored', action.locale);
      }
      break;
    }

  }

});


/**
 * If this user defined a language previously, load that. Otherwise,
 * get the locale used by the browser and load it if there's a translation
 * available. If nothing matches, the default locale is used.
 */
function browserLocale() {
  var lang;
  lang = navigator.languages || [navigator.language || navigator.userLanguage];
  /* Consider only the top 3 language preferences. */
  return lang.slice(0, 3);
}

_loadInitialLocale(browserLocale());


module.exports = LocaleStore;
