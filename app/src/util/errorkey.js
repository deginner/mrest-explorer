"use strict";


function degletErrorKey(error) {
  var errkey;

  switch (error.error) {

    case 'BADID': {
      errkey = 'error.deglet.bad_id';
      break;
    }

    case 'UNKID': {
      errkey = 'error.deglet.unknown_id';
      break;
    }

    case 'UNKPAIR': {
      errkey = 'error.deglet.unknown_pair';
      break;
    }

    case 'ADDRINUSE': {
      errkey = 'error.deglet.address_in_use';
      break;
    }

    case 'BADADDR': {
      errkey = 'error.invalid_address';
      break;
    }

    case 'SEND': {
      errkey = 'error.invoice.external_fail';
      break;
    }

    case 'STATEUPDATE': {
      errkey = 'error.deglet.state_update';
      break;
    }

    case 'UNKSIDE': {
      errkey = 'error.deglet.unknown_side';
      break;
    }

    case 'UNKCUR': {
      errkey = 'error.deglet.unknown_currency';
      break;
    }

    case 'NOCONV': {
      errkey = 'error.deglet.no_conversion';
      break;
    }

    case 'STARTED': {
      errkey = 'error.deglet.already_started';
      break;
    }

    case 'BADPAY': {
      errkey = 'error.deglet.bad_payment';
      break;
    }

    case 'BADSTATE': {
      errkey = 'error.deglet.bad_state';
      break;
    }

    case 'TOOLOW': {
      errkey = 'error.amount_too_low';
      break;
    }

    case 'LOWBAL': {
      errkey = 'error.balance.insufficient';
      break;
    }

    case 'UNKUSER': {
      errkey = 'error.deglet.unknown_user';
      break;
    }

    case 'EXIST': {
      errkey = 'error.deglet.user_already_exists';
      break;
    }

    case 'COMPLETE': {
      errkey = 'error.deglet.wallet_complete';
      break;
    }

    case 'NOBLOB': {
      errkey = 'error.deglet.no_blob';
      break;
    }

    case 'COSIGN': {
      errkey = 'error.deglet.cosign';
      break;
    }

    case 'JOIN': {
      errkey = 'error.deglet.join_cosigner';
      break;
    }

    default: {
      errkey = 'error.unknown';
      break;
    }

  }

  return errkey;
}



module.exports = {
  deglet: degletErrorKey
};
