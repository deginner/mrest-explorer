"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');

var UserStore = require('../store/UserStore');
var LocaleStore = require('../store/LocaleStore');
var template = require('../../template/App.jsx');
var debug = require('../debug')(__filename);


var ExampleApp = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.State],

  getInitialState: function() {
    return UserStore.getData();
  },

  componentDidMount: function() {
    debug('mounted');
    UserStore.addChangeListener(this._onChange);
    LocaleStore.addChangeListener(this._onLocaleChange);
  },

  componentWillUnmount: function() {
    debug('unmounting');
    UserStore.removeChangeListener(this._onChange);
    LocaleStore.removeChangeListener(this._onLocaleChange);
  },

  componentWillUpdate: function() {
    /* Make sure the mobile menu is always closed
     * when moving to a new page. */
    this.refs.mobileMenu.closeMenu();
  },

  _onChange: function() {
    this.setState(UserStore.getData());
  },

  _onLocaleChange: function() {
    ExampleApp.appRender();
  },

  render: template

});


ExampleApp.CurrentHandler = null;
ExampleApp.appRender = function() {
  /* Re-render the app. This is used to switch locales. */
  debug('appRender');
  if (ExampleApp.CurrentHandler) {
    React.render(<ExampleApp.CurrentHandler {...LocaleStore.getLocale()} />,
                 document.body);
  }
};


module.exports = ExampleApp;
