"use strict";

var React = require('react');
var template = require('../../../template/widget/BoxMessage.jsx');


var BoxMessage = React.createClass({

  propTypes: {
    error: React.PropTypes.bool,     /* Error or info message? */
    msg: React.PropTypes.node,       /* The formatted message. */
    rawMsg: React.PropTypes.string,  /* Message before special formatting. */
    onHide: React.PropTypes.func.isRequired  /* Erase msg to hide this box. */
  },

  shouldComponentUpdate: function(nextProps) {
    /* Not implementing this function for this widget causes
     * a lot of re-renders for it.
     */
    if (nextProps.error !== this.props.error ||
        nextProps.rawMsg !== this.props.rawMsg) {
      return true;
    }
    return false;
  },

  render: template

});


module.exports = BoxMessage;
