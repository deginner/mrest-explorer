"use strict";

var React = require('react');
var ClickOutside = require('react-onclickoutside');


var DropdownWidget = React.createClass({

  mixins: [ClickOutside],

  propTypes: {
    title: React.PropTypes.string,
    options: React.PropTypes.arrayOf(React.PropTypes.shape(
      {
        value: React.PropTypes.string.isRequired,
        label: React.PropTypes.string.isRequired
      }
    )).isRequired
  },

  getInitialState: function() {
    return {
      active: false,
      selected: {}
    };
  },

  _toggle: function(event) {
    var target = event.target;

    if (target !== React.findDOMNode(this.refs.dropdown) &&
        target !== React.findDOMNode(this.refs.right) &&
        target !== React.findDOMNode(this.refs.title)) {

      /* Clicked on some item. */
      if (typeof target.checked !== 'undefined' && target.value) {
        var selected = this.state.selected;
        selected[target.value] = target.checked;
        this.setState({selected: selected});
      }

      return;
    }

    this.setState({active: !this.state.active});
  },

  handleClickOutside: function() {
    this.setState({active: false});
  },

  getSelected: function() {
    var selection = [];
    Object.keys(this.state.selected).forEach(function(key) {
      if (this.state.selected[key]) {
        selection.push(key);
      }
    }, this);
    return selection;
  },

  render: function() {
    var icon;
    var className = "wrapper-dropdown";
    if (this.state.active) {
      className += " active";
      icon = "icon-up-open";
    } else {
      icon = "icon-down-open";
    }

    return (
      <div ref="dropdown" tabIndex="0" onClick={this._toggle} className={className}>
        <span ref="title">{this.props.title || "Select.."}</span>
        <span ref="right" className={"right " + icon}></span>
        <ul className="dropdown">
          {
            this.props.options.map(function(item) {
              return (
                <li key={"item-" + item.value}>
                  <div className="item">
                    <label>
                      <input type="checkbox" value={item.value}/>
                      {item.label}
                    </label>
                  </div>
                </li>
              );
            })
          }
        </ul>
      </div>
    );
  }

});


module.exports = DropdownWidget;
