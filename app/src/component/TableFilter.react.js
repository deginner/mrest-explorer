"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var template = require('../../template/TableFilter.jsx');


var TableFilter = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  propTypes: {
    show: React.PropTypes.bool.isRequired,
    filtering: React.PropTypes.bool.isRequired,
    onFilter: React.PropTypes.func.isRequired,
    dateFilter: React.PropTypes.bool,
    typeFilter: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      fromDate: null,
      toDate: null
    };
  },

  componentWillUnmount: function() {
    if (this.refs.dayFrom) {
      this.refs.dayFrom.destroy();
    }
    if (this.refs.dayTo) {
      this.refs.dayTo.destroy();
    }
  },

  clear: function() {
    this.setState({fromDate: null, toDate: null});
  },

  _onSubmit: function(event) {
    event.preventDefault();

    var txidQuery = React.findDOMNode(this.refs.txidQuery).value;
    var txActions = [];
    var dateRange = null;
    var filterCfg = {};

    if (this.props.typeFilter) {
      txActions = this.refs.typeRestr.getSelected();
    }
    if (this.state.fromDate !== null || this.state.toDate !== null) {
      /* Convert to regular unix timestamp and get the next
       * day for the upper range. */
      var toDate = this.state.toDate;
      if (toDate) {
        toDate.setDate(toDate.getDate() + 1);
        toDate = toDate / 1000;
      }
      dateRange = {
        from: this.state.fromDate ? this.state.fromDate / 1000 : null,
        to: toDate
      };
    }

    if (!txidQuery && !txActions.length && !dateRange) {
      /* Nothing to filter for. */
      return;
    }

    if (txidQuery) {
      filterCfg.txid = txidQuery;
    }
    if (txActions.length) {
      filterCfg.action = txActions;
    }
    if (dateRange) {
      filterCfg.time = dateRange;
    }

    this.props.onFilter(filterCfg);
  },

  _onFromDate: function(date) {
    this.setState({fromDate: date}, this._adjustDates);
  },

  _onToDate: function(date) {
    this.setState({toDate: date}, this._adjustDates);
  },

  _onDateManual: function(event) {
    var key = event.target.name;
    if (!event.target.value) {
      var update = {};
      update[key] = null;
      this.setState(update);
    }
  },

  _adjustDates: function() {
    if (!this.state.fromDate || !this.state.toDate) {
      return;
    }
    if (this.state.fromDate > this.state.toDate) {
      /* "From date" is ahead of "To Date", switch them. */
      var fromDate = this.state.fromDate;
      var toDate = this.state.toDate;
      this.setState({fromDate: toDate, toDate: fromDate});
    }
  },

  render: template

});


module.exports = TableFilter;
