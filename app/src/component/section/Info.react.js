"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var InfoStore = require('../../store/InfoStore');
var template = require('../../../template/section/Info.jsx');


var InfoSection = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  getInitialState: function() {
    var info = InfoStore.getData();
    return info;
  },

  componentDidMount: function() {
    InfoStore.addChangeListener(this._onChange);
  },

  componentWillUnmount: function() {
    InfoStore.removeChangeListener(this._onChange);
  },

  _onChange: function() {
    var serverInfo = InfoStore.getData();
    this.setState(serverInfo);
  },

  render: template

});


module.exports = InfoSection;
