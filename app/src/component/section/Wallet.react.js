"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');

var UserStore = require('../../store/UserStore');
var template = require('../../../template/section/Wallet.jsx');


var Wallet = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.Navigation],

  getInitialState: function() {
    return UserStore.getData();
  },

  _registerProtoAvail: function() {
    return (typeof window.navigator.registerProtocolHandler === "function");
  },

  _hideNewUser: function(event) {
    event.preventDefault();
    UserStore.noNewUser();
    this.setState({newuser: false});
  },

  _showTip: function(event) {
    event.preventDefault();
    this.refs.tipModal.show();
  },

  _hideTip: function(event) {
    event.preventDefault();
    this.refs.tipModal.hide();
  },

  render: template

});


module.exports = Wallet;
