"use strict";

var React = require('react');
var ReactIntl = require('react-intl');
var UserStore = require('../../store/UserStore');
var AppLinks = require('../../constant/AppLinks');
var template = require('../../../template/section/Welcome.jsx');


var WelcomeSection = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  statics: {
    willTransitionTo: function(transition) {
      var user = UserStore.getData().username;
      if (user) {
        /* User logged in, do not show this welcome section. */
        transition.redirect(AppLinks.name.info);
      }
    }
  },

  render: template

});


module.exports = WelcomeSection;
