"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var SignupAction = require('../../action/SignupAction');
var UserStore = require('../../store/UserStore');
var AppLinks = require('../../constant/AppLinks');
var template = require('../../../template/section/SignupForm.jsx');
var debug = require('../../debug')(__filename);


var SignupForm = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  contextTypes: {
    router: React.PropTypes.func
  },

  getInitialState: function() {
    return {
      username: '',
      password: '',
      passwordrepeat: '',
      waiting: false,
      error: false,
      info: ''
    };
  },

  componentDidMount: function() {
    UserStore.addChangeListener(this._onChange);

    /* Auto focus on not-so-small screens. */
    var width = window.innerWidth || 1000;
    if (width >= 768) {
      var node = React.findDOMNode(this.refs.username);
      node.focus();
      node.select();
    }
  },

  componentWillUnmount: function() {
    UserStore.removeChangeListener(this._onChange);
  },

  /**
   * Locally send form as a 'submit' action.
   */
  _submit: function(event) {
    event.preventDefault();
    debug('form submit');
    this.setState({submitted: true, info: '', error: false}, function() {
      SignupAction.submit(this.state);
    });
  },

  /**
   * Update the state based on the latest input value.
   */
  _update: function(event) {
    var update = {};
    update[event.target.name] = event.target.value;
    this.setState(update);
  },

  _hideInfo: function() {
    this.setState({info: '', error: false});
  },

  /**
   * Store update. Note that this update is only received after
   * a login attempt (successful or not). Therefore, at this point
   * it's safe to assume that the form submit completed.
   */
  _onChange: function() {
    if (!this.isMounted()) {
      /* In case of a successful login, a page transition happens
       * and at that point this component may be gone. */
      return;
    }
    var status = UserStore.getData().status;

    this.setState({
      waiting: status.error ? false : true,
      info: status.info,
      error: status.error
    });
    if (!status.error && !status.info) {
      /* Login ok, show server info page. */
      this.context.router.replaceWith(AppLinks.name.info);
    }
  },

  _onHideMessage: function(event) {
    event.preventDefault();
    this.setState({error: false, info: ''});
  },

  render: template

});

module.exports = SignupForm;
