"use strict";

var React = require('react');
var ReactIntl = require('react-intl');

var LocaleAction = require('../../action/LocaleAction');
var template = require('../../../template/section/Footer.jsx');


var FooterSection = React.createClass({

  mixins: [ReactIntl.IntlMixin],

  _setLocale: function(code, event) {
    event.preventDefault();
    LocaleAction.change(code);
  },

  render: template

});


module.exports = FooterSection;
