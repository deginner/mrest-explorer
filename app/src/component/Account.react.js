"use strict";

var React = require('react');
var Router = require('react-router');
var ReactIntl = require('react-intl');
var UserStore = require('../store/UserStore');
var debug = require('../debug')(__filename);

var template = require('../../template/Account.jsx');
var indexLink = require('../constant/AppLinks').name.index;


var Account = React.createClass({

  mixins: [ReactIntl.IntlMixin, Router.State],

  statics: {
    willTransitionTo: function(transition) {
      var user = UserStore.getData().username;
      if (!user) {
        /* User not logged in, send back to the initial page. */
        transition.redirect(indexLink);
      }
    }
  },

  getInitialState: function() {
    return {
      user: UserStore.getData()
    };
  },

  componentDidMount: function() {
    debug('mounted');
  },

  componentWillUnmount: function() {
    debug('unmounting');
  },

  _onNotification: function() {
  },

  _onChange: function() {
    debug('updated');
  },

  render: template

});


module.exports = Account;
