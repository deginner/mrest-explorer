"use strict";

var AppDispatcher = require('../dispatcher');
var pwdMinlen = require('../constant/App').pwdMinlen;
var bitcore = require('bitcore');
var MREST_CONFIG = require('../constant/ServerConstants').MREST_CONFIG;
var mrest = require('mrest-api-client');


var SignupAction = {

  submit: function(data) {
    /* Sign up. */
    if (data.password.length < pwdMinlen) {
      SignupAction.status({info: 'login.weakpwd', error: true});
      return;
    }

    if (data.password !== data.passwordrepeat) {
      SignupAction.status({info: 'error.login.passwordmismatch', error: true});
      return;
    }

    SignupAction.status({info: 'login.generatingkey'});

    setTimeout(function() {
      SignupAction.status({info: 'login.submitting'});

      var hexid = new Buffer(data.username + data.password).toString('hex');
      MREST_CONFIG.privkey = new bitcore.PrivateKey(hexid);
      var pubHash = MREST_CONFIG.privkey.toAddress().toString();
      var mrestClient = mrest(MREST_CONFIG);
      mrestClient.update_server_info({}, function(er) {
        if (er) {
          SignupAction.status({info: 'error.login.server_comm', error: true});
          return;
        }
        SignupAction.gotInfo(mrestClient.raw_info);
        mrestClient.post({model: 'user', data: {'pubhash': pubHash, 'username': data.username}}, function(err, resp) {
          if (err) {
            SignupAction.status({info: 'error.login.creds', error: true});
          } else {
            SignupAction.loginOk({
              username: data.username,
              uid: resp.id,
              pubhash: pubHash,
              privkey: MREST_CONFIG.privkey,
              mrestClient: mrestClient
            });
          }
        });
      });
    }, 250);
  },

  gotInfo: function(data) {
    data.actionType = 'info-load';
    AppDispatcher.dispatch(data);
  },

  loginOk: function(data) {
    data.actionType = 'login-ok';
    data.newuser = true;
    AppDispatcher.dispatch(data);
  },

  status: function(data) {
    data.actionType = 'user-status';
    AppDispatcher.dispatch(data);
  },

  logout: function() {
    AppDispatcher.dispatch({actionType: 'logout'});
  }

};


module.exports = SignupAction;
