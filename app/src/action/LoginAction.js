"use strict";

var AppDispatcher = require('../dispatcher');
var bitcore = require('bitcore');
var MREST_CONFIG = require('../constant/ServerConstants').MREST_CONFIG;
var mrest = require('mrest-api-client');

var LoginAction = {

  submit: function(data) {
    /* Login. */
    LoginAction.status({info: 'login.generatingkey'});

    setTimeout(function() {
      LoginAction.status({info: 'login.submitting'});

      var hexid = new Buffer(data.username + data.password).toString('hex');
      MREST_CONFIG.privkey = new bitcore.PrivateKey(hexid);
      var pubHash = MREST_CONFIG.privkey.toAddress().toString();
      var mrestClient = mrest(MREST_CONFIG);
      mrestClient.update_server_info({'accept_keys': true}, function(er) {
        if (er) {
          LoginAction.status({info: 'error.login.server_comm', error: true});
          return;
        }
        LoginAction.gotInfo(mrestClient.raw_info);
        mrestClient.get({model: 'user', id: pubHash}, function(err, resp) {
          if (err) {
            LoginAction.status({info: 'error.login.creds', error: true});
          } else {
            LoginAction.loginOk({
              username: data.username,
              uid: resp.id,
              pubhash: pubHash,
              privkey: MREST_CONFIG.privkey,
              mrestClient: mrestClient
            });
          }
        });
      });
    }, 250);
  },

  gotInfo: function(data) {
    data.actionType = 'info-load';
    AppDispatcher.dispatch(data);
  },

  loginOk: function(data) {
    data.actionType = 'login-ok';
    AppDispatcher.dispatch(data);
  },

  status: function(data) {
    data.actionType = 'user-status';
    AppDispatcher.dispatch(data);
  },

  logout: function() {
    AppDispatcher.dispatch({actionType: 'logout'});
  }

};


module.exports = LoginAction;
