"use strict";

var AppDispatcher = require('../dispatcher');
var debug = require('../debug')(__filename);

var LocaleAction = {

  change: function(code) {
    debug('dispatching action "change-locale" ' + code);
    AppDispatcher.dispatch({
      actionType: 'change-locale',
      locale: code
    });
  }

};

module.exports = LocaleAction;
