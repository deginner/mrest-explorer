"use strict";

var React = require('react');
var Router = require('react-router');
var Route = Router.Route;

var debug = require('debug');
var Constants = require('./src/constant/App');
debug.enable(Constants.log.ns + ':*');

var AppLinks = require('./src/constant/AppLinks');
var App = require('./src/component/App.react');
var WelcomeSection = require('./src/component/section/Welcome.react');
var Account = require('./src/component/Account.react');
var NotFound = require('./src/component/section/NotFound.react');

var InfoSection = require('./src/component/section/Info.react');
var CreateSection = require('./src/component/section/Create.react');
var LoginForm = require('./src/component/section/LoginForm.react');
var SignupForm = require('./src/component/section/SignupForm.react');
var Logout = require('./src/component/auth/Logout.react');


var routes = (
  <Route handler={App}>
    <Route name={AppLinks.name.index} path="/" handler={WelcomeSection}>
      <Router.DefaultRoute name="signup" handler={SignupForm}/>
      <Route name="login" handler={LoginForm}/>
    </Route>
    <Route handler={Account}>
      <Route name={AppLinks.name.info} handler={InfoSection}/>
      <Route name="create" handler={CreateSection}/>
      <Route name="logout" handler={Logout}/>
    </Route>
    <Router.NotFoundRoute handler={NotFound} />
  </Route>
);

var AppRouter = Router.create({
  routes: routes,
  location: Router.HistoryLocation
});

AppRouter.run(function(Handler) {
  App.CurrentHandler = Handler;
  App.appRender();
});
