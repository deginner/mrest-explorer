/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var LaddaButton = require('react-ladda');
var Dropdown = require('../src/component/widget/Dropdown.react');
var Pikaday = require('../src/component/widget/DayPicker.react');


/* Labels are loaded based on the active locale. */
var typeOptions = [
  {key: 'transaction.type.received', value: 'received'},
  {key: 'transaction.type.sent', value: 'sent'},
  {key: 'transaction.type.moved', value: 'moved'}
];


function _filterByType(options, i18n) {
  for (var i = 0; i < options.length; i++) {
    var option = options[i];
    option.label = i18n(option.key);
  }

  return (
    <div className="col-xs-8 col-sm-4 first-xs last-sm sm-margin-bottom">
      <Dropdown ref="typeRestr" title={i18n('history.filterBy.type')} options={options} />
    </div>
  );
}


function _filterByDateRange(component, allFilters) {
  var now = new Date();

  return (
    <div className={"col-xs-12 col-sm-4 last-sm " + (allFilters ? "margin-top-1" : "sm-margin-top")}>
      <div className="row">
        <div className="col-xs-6">
          <Pikaday ref="dayFrom" name="fromDate" value={component.state.fromDate}
                   maxDate={now} onChange={component._onFromDate}
                   onManualChange={component._onDateManual}
                   placeholder={component.getIntlMessage('history.filterBy.fromDate')} />
        </div>
        <div className="col-xs-6">
          <Pikaday ref="dayTo" name="toDate" value={component.state.toDate}
                   maxDate={now} onChange={component._onToDate}
                   onManualChange={component._onDateManual}
                   placeholder={component.getIntlMessage('history.filterBy.toDate')} />
        </div>
      </div>
    </div>
  );
}


function template() {
  if (!this.props.show) {
    return null;
  }

  var dateFilter = null;
  var typeFilter = null;
  var allFilters = this.props.typeFilter && this.props.dateFilter;

  if (this.props.typeFilter) {
    typeFilter = _filterByType(typeOptions, this.getIntlMessage);
  }
  if (this.props.dateFilter) {
    dateFilter = _filterByDateRange(this, allFilters);
  }

  return (
    <div className="table-filter row">
      <div className="col-xs">
        <form onSubmit={this._onSubmit}>
          <div className="row">

            <div className="col-xs-12 col-sm-6 col-md-6">
              <input type="text" ref="txidQuery" pattern=".{2,}"
                     placeholder={this.getIntlMessage('history.filterBy.txid')}/>
            </div>

            {typeFilter}
            {dateFilter}

            <div className={"col-xs-4 col-sm-2 last-sm " + (allFilters ? "margin-top-1" : "sm-margin-top")}>
              <LaddaButton buttonStyle="zoom-in" loading={this.props.filtering}
                           type="submit" className="button button-full action-button">
                {this.getIntlMessage('history.button.filter')}
              </LaddaButton>
            </div>

          </div>
        </form>
      </div>
    </div>
  );
}


module.exports = template;
