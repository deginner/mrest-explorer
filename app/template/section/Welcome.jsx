/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;

function template() {
  var t = this.getIntlMessage;

  return (
    <section className="row">

      <div className="col-xs-12 col-sm-11 col-sm-offset-1 col-md-5 col-lg-6">
        <div className="welcome-header">
          <h1 className="title-1">
            {t('welcome.title')}
          </h1>
          <h4 className="title-4">
            {t('welcome.subtitle')}
          </h4>
        </div>
      </div>

      <div className="col-xs-12 col-sm-7 col-sm-offset-1 col-md-5 first-md col-lg-4">
      <div className="loginarea">
        <h3 className="title-3">{t('login.title')}</h3>
        <h4 className="title-4 row">
          <div className="col-xs">
            {t('login.subtitle')}
          </div>
        </h4>
        <div>
          <div className="user-tabs">
            <h4><Link to="signup">{this.getIntlMessage('link.signup')}</Link></h4>
            <h4><Link to="login">{this.getIntlMessage('link.login')}</Link></h4>
          </div>
          <hr className="hr-tabs"/>
          <Router.RouteHandler {...this.props} />
        </div>
      </div>
      </div>

      <div className="col-xs-12 col-sm-11 col-sm-offset-1 col-md-5 col-md-offset-7 col-lg-offset-6">
        <div className="welcome-feature">
          <ul>
            <li>{t('welcome.feature_explorer_1')}</li>
            <li>{t('welcome.feature_explorer_2')}</li>
            <li>{t('welcome.feature_explorer_3')}</li>
          </ul>
        </div>
      </div>

    </section>
  );
}


module.exports = template;
