/*jscs:disable maximumLineLength */
"use strict";


var React = require('react');
var Link = require('react-router').Link;

function template() {
  var t = this.getIntlMessage;

  function methodRuleFormat(method, model, schema) {
    var response;
    if (schema.routes['/'][method] !== undefined) {
      response = (
        <td className="find">
          <Link to="create" params={{model: model}}>{t("info.find.many")}</Link>
        </td>
      );
    } else if (schema.routes['/:id'][method] !== undefined) {
      response = (
        <td className="find">
          <Link to="create" params={{model: model}}>{t("info.find.one")}</Link>
        </td>
      );
    } else {
      response = (
        <td className="forbidden">{t("info.forbidden")}</td>
      );
    }
    return response;
  }

  function schemaRowFormat(model, schema) {
    var key = model + "-schema";
    var routeRules = {'one': {}, 'many': {}};
    ['GET', 'POST', 'PUT', 'DELETE'].map(function(method) {
      routeRules[method] = methodRuleFormat(method, model, schema);
    });
    console.log(routeRules);
    return (
      <tr key={key}>
        <td className="model">{model}</td>
        <td>{schema.description}</td>
        {routeRules.GET}
      </tr>
    );
  }

  function formatSchemas(entry) {
    var trs = [];
    for (var model in entry.state.schemas) {
      trs.push(schemaRowFormat(model, entry.state.schemas[model]));
    }
    return trs;
  }

  return (
    <section className="row">

      <div className="col-xs-12 col-sm-11 col-sm-offset-1 col-md-5 col-lg-6">
        <div className="info-header">
          <h1 className="title-1">
            {this.state.name} {t('info.title')}
          </h1>
        </div>
      </div>
      <div className="col-xs-12 col-sm-11 col-sm-offset-1 col-md-5 col-lg-6">
        <h4 className="title-4">
          {t('info.url')}
        </h4>
      </div>
      <div className="col-xs-12 col-sm-11 col-sm-offset-1 col-md-5 col-lg-6">
        <h4 className="title-4">
          {t('info.schemas')}
        </h4>
        <table className="schema-table">
          <thead>
            <tr>
              <th>Model</th>
              <th>Description</th>
              <th>Find</th>
            </tr>
          </thead>
          <tbody className={this.state.loading ? "loading" : null}>
            {formatSchemas(this)}
          </tbody>
        </table>
      </div>
    </section>
  );
}


module.exports = template;
