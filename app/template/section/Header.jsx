/*jscs:disable maximumLineLength */
"use strict";

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var classNames = require('classnames');

var Logo = require('../../src/component/Logo.react');
var AppLinks = require('../../src/constant/AppLinks');


function template() {
  var tmpl;
  var loggedin = this.props.username;

  if (this.getQuery().header === '0') {
    /* Do not render the header. */
    return null;
  }

  function link(item) {
    if (item.parent !== null) {
      /* Not a top level link. */
      return null;
    }

    return (
      <Link to={item.link} key={item.link + "-header"}
            className={classNames({active: item.link === AppLinks.name.info})}>
        {this.getIntlMessage(item.i18n)}
      </Link>
    );
  }

  if (loggedin) {
    /* Include navigation links for a user that is logged in. */
    tmpl = (
      <div ref="menuLinks" className="menu-links">
        {AppLinks.list.map(link, this)}
      </div>
    );
  } else {
    tmpl = <p className="thin">{this.getIntlMessage('header.title')}</p>;
  }

  return (
    <header className={classNames({user: loggedin})}>
    <div className='logo-bg'>
      <Link to={AppLinks.name.index} className="logo-area logo-small logo-light">
        <Logo />
      </Link>
    </div>
    <div className="right-logo">
      {tmpl}
    </div>
    </header>
  );
}

module.exports = template;
