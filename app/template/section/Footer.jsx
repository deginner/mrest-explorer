"use strict";

var React = require('react');

var Logo = require('../../src/component/Logo.react');
var LocaleStore = require('../../src/store/LocaleStore');


function template() {
  var lang = LocaleStore.localeOrder();
  var currentLocale = LocaleStore.getLocale().code;

  var langLinks = lang.map(function(entry) {
    var title = this.getIntlMessage('lang.' + entry.code + '_name');
    return (<a href="#" key={entry.code}
               className={currentLocale === entry.code ? "active" : ""}
               onClick={this._setLocale.bind(this, entry.code)}
               title={title}>{entry.name}</a>);
  }, this);

  return (
    <footer>
    <div className="footer">
      <div className="lang">
        {langLinks}
      </div>
      <div className="right-bottom logo-normal logo-dark">
        <Logo />
      </div>
    </div>
   </footer>
  );
}

module.exports = template;
