"use strict";

var React = require('react');
var Link = require('react-router').Link;
var InfoLink = require('../../src/constant/AppLinks').name.info;


function template() {

  return (
    <div>
      <p>{this.getIntlMessage('page_notfound.message')}</p>
      <Link to={InfoLink}>{this.getIntlMessage('page_notfound.info_link')}</Link>
    </div>
  );

}


module.exports = template;
