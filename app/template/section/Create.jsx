/*jscs:disable maximumLineLength */
"use strict";


var React = require('react');

function template() {

  return (
    <section className="row">
      <div className="col-xs-12 col-sm-11 col-sm-offset-1 col-md-5 col-lg-6">
        <div className="info-header">
          <h1 className="title-1">
            {this.state.name}
          </h1>
        </div>
      </div>
    </section>
  );
}


module.exports = template;
