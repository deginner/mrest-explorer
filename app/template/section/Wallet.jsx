/*jscs:disable maximumLineLength */

"use strict";

var React = require('react');
var FmtMsg = require('react-intl').FormattedMessage;
var Modal = require('boron/FadeModal');


function template() {
  var newuser = null;

  if (this.state.newuser) {
    newuser = (
      <div>
        <div className="separator-m"></div>
        <p className="small-margin newuser-title">
          {this.getIntlMessage('widget.newuser.title')}
        </p>
        <div className="newuser">
          <p>{this.getIntlMessage('generic.tip')}: <span className="tip">
              {this.getIntlMessage('widget.newuser.tip')}</span> <a
              href="#" className="action-button" onClick={this._showTip} title={this.getIntlMessage('generic.learn_more')}>
              <span className="icon-help"></span>
            </a>
          </p>
          <a href="#" className="button action-button dark-button text-heavy" onClick={this._hideNewUser}>
            {this.getIntlMessage('widget.newuser.accept')}
          </a>
        </div>
        <Modal ref="tipModal" className="tip-modal">
          <div className="tip-modal-wrapper">
            <h3>{this.getIntlMessage('generic.tip')}</h3>
            <p className="tip">{this.getIntlMessage('widget.newuser.tip')}</p>
            <p className="tip-long">{this.getIntlMessage('widget.newuser.tip_long')}</p>
            <a href="#" onClick={this._hideTip}>{this.getIntlMessage('generic.close')}</a>
          </div>
        </Modal>
      </div>
    );
  }

  return (
    <div>
      <p>
        <FmtMsg
          message={this.getIntlMessage('welcome.user')}
          name={this.props.login.username} />
      </p>
      <hr/>

      <div className="row">
        <div className="col-xs-12 col-sm-8 col-lg-8">

          <div className="row">
            <div className="col-xs-12 col-sm-10 col-md-8 col-lg-6">
              <div className="separator-m"></div>

            </div>
            <div className="col-xs-12 col-md-4 col-lg-5 col-lg-offset-1 first-xs last-md">
              {/* Reminder for new users. */}
              {newuser}
            </div>
          </div>
        </div>

        {/* Right side */}
        <div className="col-xs-12 col-sm-4 col-lg-3 col-lg-offset-1 first-xs last-sm">
          <div className="separator-m"></div>
        </div>

      </div>

    </div>
  );
}


module.exports = template;
