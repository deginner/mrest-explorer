"use strict";

var React = require('react');
var Paginate = require('react-paginate');
var classNames = require('classnames');


function paginate(selectedPage, pageCount, callback) {
  if (pageCount <= 1) {
    return null;
  }

  return (
    <Paginate
      previousLabel={String.fromCharCode(12296)}
      nextLabel={String.fromCharCode(12297)}
      marginPagesDisplayed={3}
      pageRangeDisplayed={5}
      clickCallback={callback}
      initialSelected={selectedPage - 1}
      forceSelected={selectedPage - 1}
      pageNum={pageCount} />
  );
}


function status(component, hist) {
  var i18n = component.getIntlMessage;
  var total = component.state.store.total;
  var infoClass = "col-sm-3 col-md-2";
  var info = null;

  if (component.state.loading) {
    info = i18n('generic.loading');
  } else if (!hist.length && !component.state.store.filtered) {
    /* No results without any filters means the user did not
     * complete any transactions yet.
     */
    info = i18n('history.empty');
  } else {
    /* Cool, there is some history. */
    info = component.formatMessage(
      i18n('history.recordtitle'),
      {num: total});

    if (total === 0) {
      infoClass = "col-sm-5 col-md-4";
    }
  }

  return (
    <div className={"col-xs-12 " + infoClass}>
      <span className="history-sub">{info}</span>
    </div>
  );
}


function filterButtons(component, histLen, onToggleFilter, onClearFilter) {
  var i18n = component.getIntlMessage;
  var showFilter = component.state.showFilter;
  var filterClass = "col-sm-9 col-md-10";
  var filterButton = null;
  var clearButton = null;

  if (!component.state.loading && (histLen || component.state.store.filtered)) {
    /* Cool, there is some history. */
    if (component.state.store.total === 0) {
      filterClass = "col-sm-7 col-md-8";
    }

    filterButton = (
      <a onClick={onToggleFilter} tabIndex="0"
         className={classNames("filter-button", {active: showFilter})}>
        <span className="history-sub">{i18n('history.button.filtertoggle')} &nbsp;</span>
        <span className={classNames(
          {"icon-down-open": !showFilter,
           "icon-up-open": showFilter})}>
        </span>
      </a>
    );
  }

  if (!component.state.loading && component.state.store.filtered) {
    /* Results were filtered, show button to remove any
     * filters in place.
     **/
    clearButton = (
      <a onClick={onClearFilter} tabIndex="0"
         className="filter-button clear-button"
         title={i18n('history.button.clearfilter')}>
        <span className="history-sub icon-trash-empty">
          <span>{i18n('history.button.clearfilter')}</span>
        </span>
      </a>
    );
  }

  return (
    <div className={"col-xs-12 sm-margin-top " + filterClass}>
      {filterButton}
      {clearButton}
    </div>
  );
}


module.exports = {
  paginate: paginate,
  status: status,
  filterButtons: filterButtons
};
