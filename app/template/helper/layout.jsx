"use strict";

var React = require('react');


function row(label, colClass, innerElem) {
  /* Create a row (or two), typically in a form.
   * Resulting layout:
   *
   * <div>
   *   <div> <!-- Only if label is specified ->
   *     <div class="row">
   *       <div class="col-xs-12">
   *         <label>label</label>
   *       </div>
   *     </div>
   *   </div>
   *   <div class="row">
   *     <div class=colClass>
   *       innerElem
   *     </div>
   *   </div>
   * </div>
   *
   **/
  var labelElem = label ? row(null, null, <label>{label}</label>) : null;
  return (
    <div>
      {labelElem}
      <div className="row">
        <div className={colClass || "col-xs-12"}>
          {innerElem}
        </div>
      </div>
    </div>
  );
}


function rowAddon(label, addon, innerElem) {
  /* Similar to row(...) but integrates an add-on to the right of innerElem.
   * Typical use case is to produce the following result:
   *  ______________ ___
   * | Input        | A |
   * |______________|___|
   *
   **/
  var addonElem;
  if (typeof addon === "string") {
    addonElem = <button type="button" className="button right-addon wireframe" disabled>{addon}</button>;
  } else {
    addonElem = addon;
  }

  return (
    <div>
      {label ? row(null, null, <label>{label}</label>) : null}
      <div className="row">
        <div className="col-xs contain-right-addon">
          {innerElem}
        </div>
        {addonElem}
      </div>
    </div>
  );
}


module.exports = {
  row: row,
  rowAddon: rowAddon
};
