"use strict";

var React = require('react');
var Router = require('react-router');
var Notification = require('../src/component/Notification.react.js');

function template() {
  return (
    <div>
      <Notification info={this.state.notification} />
      <Router.RouteHandler {...this.state} />
    </div>
  );
}


module.exports = template;
