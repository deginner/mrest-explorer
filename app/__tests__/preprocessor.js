/* Used for testing JSX with Jest. */

"use strict";

var ReactTools = require('react-tools');
module.exports = {
  process: function(src, filename) {
    if (!/(\.react.js|\.jsx)$/.test(filename)) {
      return src;
    }

    return ReactTools.transform(src);
  }
};
