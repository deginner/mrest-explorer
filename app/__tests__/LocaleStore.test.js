"use strict";

/*global jest, describe, beforeEach, it, expect */

jest.dontMock('../locale/messages');
jest.dontMock('../src/debug');
jest.dontMock('../src/store/storage');
jest.dontMock('../src/store/LocaleStore');


describe('LocaleStore', function() {

  var LocaleStore;
  var Dispatcher;
  var callback;

  beforeEach(function() {
    Dispatcher = require('../src/dispatcher');
    LocaleStore = require('../src/store/LocaleStore');
    callback = Dispatcher.register.mock.calls[0][0];
  });

  it('changes the active locale', function() {
    var action = {
      actionType: 'change-locale',
      locale: null
    };

    var locales = LocaleStore.localeOrder();
    var currentLocale = LocaleStore.getLocale().code;
    var newLocale = currentLocale;

    /* Get a locale code that is != from the current one. */
    for (var i = 0; i < locales.length; i++) {
      if (locales[i].code !== currentLocale) {
        newLocale = locales[i].code;
        break;
      }
    }
    expect(newLocale).not.toEqual(currentLocale);

    /* Change locale. */
    action.locale = newLocale;
    callback(action);
    expect(LocaleStore.getLocale().code).toEqual(newLocale);
  });

  it('keeps the same locale if changing to the current one', function() {
    var currentLocale = LocaleStore.getLocale().code;
    var action = {
      actionType: 'change-locale',
      locale: currentLocale
    };

    /* "Changing" to the same locale shouldn't affect it. */
    callback(action);
    expect(LocaleStore.getLocale().code).toEqual(currentLocale);
  });

  it('does not change the locale if it does not exist', function() {
    var currentLocale = LocaleStore.getLocale().code;
    var action = {
      actionType: 'change-locale',
      locale: 'xyz'
    };

    callback(action);
    expect(LocaleStore.getLocale().code).toEqual(currentLocale);
  });

  it('fires an event when the locale changes', function() {
    var action = {
      actionType: 'change-locale',
      locale: 'ptBR'
    };

    var invoked = false;

    function cb() {
      invoked = true;
    }

    LocaleStore.addChangeListener(cb);
    callback(action);
    expect(invoked).toEqual(true);
    LocaleStore.removeChangeListener(cb);
  });

});
