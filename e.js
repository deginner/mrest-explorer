"use strict";

var express = require('express');
var path = require('path');
var app = express();

app.use('/', express.static(path.join(__dirname, '/public')));

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, '/public/html/index.html'));
});

var port = 8118;
app.listen(port);
console.log('listening on port', port);
