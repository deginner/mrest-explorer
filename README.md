# MREST Explorer
A generic explorer for MREST servers. Lets the user manage keys and servers, as well as read, write, update and delete items.

## Local setup

1. `npm install`
3. `make all`
4. Run `node e.js` and then visit `http://localhost:8118`.

To get a fully working version, a server-side component (described below) is required. The default settings expect a server running under 127.0.0.1:6367, to change that adjust `app/src/constant/ServerConstants.js` and then run `make` (after that reload the website).

For now node 0.10.x is required to run `npm test`. If `grunt` is not available, run `npm install -g grunt-cli`.
